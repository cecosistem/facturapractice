//  funcion template para head de la factura
function templateFact(espacio, product, cant, precio, totales) {
  const template = `
  <div>
  <table style="width: 79%">
     <h1>${espacio}</h1>
     <tr>
        <th>${product}</th>
        <th>${cant}</th>
        <th>${precio}</th>
        <th>${totales}</th> 
     </tr>
    </table>
  </div>`;
  return template;
}

//TEMPLATE DE LA FACTURA
function getFacturaTemplate(factura, espacio, editable) {
  // factura = facturaTemporal();
  const headFactura = templateFact(
    espacio,
    "Producto",
    "Cantidad",
    "Precio",
    "Total"
  );
  let rows = "";
  let bntCerrar = `<button id="cerrar" onclick="insertFact(factura)">Cerrar Factura</button>`;
  for (let i = 0; i < factura.productos.length; i++) {
    const product = factura.productos[i];
    const row = getProductTemplate(product, i, editable);
    rows = rows + row;
  }
  if (editable == false) {
    bntCerrar = "";
  }

  const template = `
  <div>
  <div>
      <tr>
        ${headFactura}
      </tr> 
      <table style="width: 100%">
      ${rows}
      <tr>
        <th colspan="3">Total</th> 
        <th>${factura.total.toFixed(2)}</th>
      </tr>
      <tr>
        <th>${bntCerrar}</th>
      </tr>
    </table>
  </div>
  </div>
 `;
  return template;
}
//TEMPLATE DE LOS PRODUCTOS
function getProductTemplate(product, position, editable) {
  let imput = `<input type="text" id="cantidad_${position}" value ="${product.cantidad}">`;
  let btnModificar = `<button name="modCantidad" onclick ="editCantidad('${product.id}', ${position})" >Modificar</button>`;
  let btnEliminar = ` <button name="delete" onclick ="deleteProduct('${product.id}', ${position})" >Eliminar</button>`;
  if (editable === false) {
    imput = `${product.cantidad}`;
    btnModificar = "";
    btnEliminar = "";
  }
  const template = `
  <tr>
    <td>${product.nombre}</td>
    <td>
      ${imput}
    </td>
    <td>${product.precio}</td>
    <td style="font-weight: bold">${(product.precio * product.cantidad).toFixed(
      2
    )}</td>
    <td>
    ${btnModificar} 
    </td>
    <td>
     ${btnEliminar}
    </td>
  </tr>
  `;
  return template;
}

//TEMPLATE DEL REPORTE X
function getTotalVentas() {
  const lista = getFacturas();
  let countProcessed = contadordefactura();
  let reportTotal = calculateTotalGlobal(lista);
  const resultList = `
  <tr>
    <hr>
    <td>${countProcessed}</td>
    <td>${reportTotal.toFixed(2)}</td>
  </tr>
  `;
  return resultList;
}

//TEMPLATE DEL REPORTE DETALLADO
function getListFactTemplate() {
  const lista = getTotalVentas();
  const listaActual = getFacturas();
  const totalCantidad = srvCantidadMap(factura);
  let rows = "";
  for (let i = 0; i < listaActual.length; i++) {
    const invoice = listaActual[i];
    const row = ListaFacturas(invoice, i);
    rows = rows + row;
  }
  const encFactura = templateFact(
    "REPORTE X",
    "NRO FACTURA",
    "NRO PRODUCTOS",
    "MONTO TOTAL",
    ""
  );
  const detailFact = `
  ${encFactura}
  <table style="width: 79%">
   <div>
    <tr>
      ${rows}
    </tr>
    <tr>
      <th>FACTURAS PROCESADAS</th>
      <th>TOTAL PROCESADO</th>
     </tr>
     <tr>
       ${lista}
     </tr>
   </div>
  </table>`;
  return detailFact;
}
// revisar listFacturas
function ListaFacturas(fact) {
  const totalproducts = srvCantidadMap(fact);
  const resultList = `
  <tr>  
    <td>COD: 00${fact.numberFact}</td>
    <td>${totalproducts}</td>
    <td>${fact.total.toFixed(2)}</td>
    <td><button onclick = "viewDetail(${
      fact.numberFact
    })">Detallado</button></td>
  </tr>
  `;
  return resultList;
}

//LA FUNCION MAS PODEROSA DEL TEMPLATE
function showTemplate(template, destino) {
  destino.innerHTML = template;
}
